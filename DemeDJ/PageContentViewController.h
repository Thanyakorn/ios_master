//
//  PageContentViewController.h
//  DemeDJ
//
//  Created by Moremo on 7/26/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *tutorialImagesView;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end
