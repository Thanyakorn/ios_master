//
//  SignupViewController.h
//  DemeDJ
//
//  Created by Moremo on 7/22/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *usernameTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;


- (IBAction)chooseSignupImage:(id)sender;

- (IBAction)signupBtn:(id)sender;

- (IBAction)backBtn:(id)sender;

@end
