//
//  SignupViewController.m
//  DemeDJ
//
//  Created by Moremo on 7/22/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "SignupViewController.h"

@interface SignupViewController ()
@end

@implementation SignupViewController

@synthesize usernameTxt;
@synthesize emailTxt;
@synthesize passwordTxt;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
}

- (void) hideKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)chooseSignupImage:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)signupBtn:(id)sender
{
    UIViewController *storyboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarController"];
    [self presentViewController:storyboard animated:YES completion:nil];
}

- (IBAction)backBtn:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
