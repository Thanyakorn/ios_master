//
//  VenuesViewController.m
//  DemeDJ
//
//  Created by Moremo on 8/5/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "VenuesViewController.h"

#import "VenuesMapViewController.h"

@interface VenuesViewController ()

@end

@implementation VenuesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)venuesMap:(id)sender
{
    VenuesMapViewController *venuesmapView = [self.storyboard instantiateViewControllerWithIdentifier:@"VenuesMapViewController"];
    
    [self presentViewController:venuesmapView animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
