//
//  DetailEventsViewController.h
//  DemeDJ
//
//  Created by Moremo on 8/4/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface EventDetailViewController : UIViewController <UIScrollViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *detailScll;

@property (strong, nonatomic) IBOutlet MKMapView *eventMap;
@property(nonatomic, retain) CLLocationManager *locationManager;

@property (strong, nonatomic) IBOutlet UILabel *nameEventlbl;
@property (strong, nonatomic) IBOutlet UILabel *dateEventlbl;
@property (strong, nonatomic) IBOutlet UILabel *timeEventlbl;
@property (strong, nonatomic) IBOutlet UILabel *priceEventlbl;
@property (strong, nonatomic) IBOutlet UILabel *locationEventlbl;
@property (strong, nonatomic) IBOutlet UILabel *telEventlbl;

@property (nonatomic, strong) NSString *nameEvent;
@property (nonatomic, strong) NSString *dateEvent;
@property (nonatomic, strong) NSString *timeEvent;
@property (nonatomic, strong) NSString *priceEvent;
@property (nonatomic, strong) NSString *latEvent;
@property (nonatomic, strong) NSString *longEvent;
@property (nonatomic, strong) NSString *locationEvent;
@property (nonatomic, strong) NSString *telEvent;

- (IBAction)backBtn:(id)sender;

- (IBAction)getTicketBtn:(id)sender;

@end

