//
//  LoginViewController.m
//  DemeDJ
//
//  Created by Moremo on 7/22/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "LoginViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController ()
@end

@implementation LoginViewController

@synthesize emailTxt;
@synthesize passwordTxt;

- (void)viewDidLoad
{
    [super viewDidLoad];

    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];

}

- (void) hideKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)loginBtn:(id)sender
{
    UIViewController *storyboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarController"];
    [self presentViewController:storyboard animated:YES completion:nil];
}

- (IBAction)backBtn:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
