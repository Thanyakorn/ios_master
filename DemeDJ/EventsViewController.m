//
//  EventsViewController.m
//  DemeDJ
//
//  Created by Moremo on 8/1/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "EventsViewController.h"

#import "EventDetailViewController.h"

@interface EventsViewController ()
@end

@implementation EventsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    event_title = @"event_title";
    event_image = @"event_image";
    event_date = @"event_date";
    event_price = @"event_price";
    event_latitude = @"event_latitude";
    event_longtitude = @"event_longtitude";
    event_location = @"event_location";
    event_tel = @"event_tel";
    base_url = @"base_url";
    
    myEvents = [[NSMutableArray alloc] init];
    
    NSString *urlString = [NSString stringWithFormat: @"http://popdj.masterofapp.com/api/events"];
    NSData *jsonData = [NSData dataWithContentsOfURL: [NSURL URLWithString:urlString]];
    
//    NSError *error;
//    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                           options:kNilOptions
//                                                             error:&error];
//    
//    NSLog(@"json: %@", json);
    
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData
                                                     options:NSJSONReadingMutableContainers error:nil];
    for (NSDictionary *dataDict in jsonObjects)
    {
        NSString *strEvent_Title = [dataDict objectForKey:@"event_title"];
        //        NSString *strEvent_Image = [dataDict objectForKey:@"event_image"];
        UIImage *strEvent_Image = [dataDict objectForKey:@"event_image"];
        NSString *strEvent_Date = [dataDict objectForKey:@"event_date"];
        NSString *strEvent_Price = [dataDict objectForKey:@"event_price"];
        NSString *strEvent_Latitude = [dataDict objectForKey:@"event_latitude"];
        NSString *strEvent_Longtitude = [dataDict objectForKey:@"event_longtitude"];
        NSString *strEvent_Location = [dataDict objectForKey:@"event_location"];
        NSString *strEvent_Tel = [dataDict objectForKey:@"event_tel"];
        NSString *strBase_URL = [dataDict objectForKey:@"base_url"];
        
        NSString *strNEvent_Price = [strEvent_Price stringByReplacingOccurrencesOfString:@".00"
                                                                              withString:@""];
        NSString *strNEvent_Date = [strEvent_Date stringByReplacingOccurrencesOfString:@"T"
                                                                              withString:@" "];
        
//        NSLog(@"%@",strNEvent_Date);
        
        
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                      strEvent_Title, event_title,
                      strEvent_Image, event_image,
                      strNEvent_Date, event_date,
                      strNEvent_Price, event_price,
                      strEvent_Latitude, event_latitude,
                      strEvent_Longtitude, event_longtitude,
                      strEvent_Location, event_location,
                      strEvent_Tel, event_tel,
                      strBase_URL, base_url,
                      nil];
        
        [myEvents addObject:dictionary];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return myEvents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"eventsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    NSDictionary *tmpDict = [myEvents objectAtIndex:indexPath.row];
    
    UILabel *eventTitleTxt = (UILabel *)[cell viewWithTag:201];
    UILabel *eventLocationTxt = (UILabel *)[cell viewWithTag:202];
    UIImageView *eventImageView = (UIImageView *)[cell viewWithTag:203];
    
    eventTitleTxt.text = [tmpDict objectForKey:event_title];
    eventLocationTxt.text = [tmpDict objectForKey:event_location];
    [eventImageView setImage:[UIImage imageNamed:@"imgHighlight"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailViewController *eventdetailView = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
    NSDictionary *tmpDict = [myEvents objectAtIndex:indexPath.row];
    
    eventdetailView.nameEvent = [tmpDict objectForKey:event_title];
    eventdetailView.dateEvent = [tmpDict objectForKey:event_date];
    eventdetailView.timeEvent = [tmpDict objectForKey:event_date];
    eventdetailView.priceEvent = [tmpDict objectForKey:event_price];
    
    eventdetailView.latEvent = [tmpDict objectForKey:event_latitude];
    eventdetailView.longEvent = [tmpDict objectForKey:event_longtitude];
    
    eventdetailView.locationEvent = [tmpDict objectForKey:event_location];
    eventdetailView.telEvent = [tmpDict objectForKey:event_tel];
    
    [self presentViewController:eventdetailView animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
