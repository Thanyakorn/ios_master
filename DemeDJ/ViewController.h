//
//  ViewController.h
//  DemeDJ
//
//  Created by Moremo on 7/15/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PageContentViewController.h"

@interface ViewController : UIViewController //<UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (strong, nonatomic) NSArray *tutorialImages;
@property (strong, nonatomic) NSArray *tutorialTitles;

- (IBAction)loginWithFacebook:(id)sender;

- (IBAction)loginWithEmail:(id)sender;

@end

