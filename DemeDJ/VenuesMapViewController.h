//
//  VenuesMapViewController.h
//  DemeDJ
//
//  Created by Moremo on 8/5/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface VenuesMapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *venuesMap;
@property(nonatomic, retain) CLLocationManager *locationManager;

- (IBAction)backBtn:(id)sender;

@end
