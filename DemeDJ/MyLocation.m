//
//  MyLocation.m
//  DemeDJ
//
//  Created by Moremo on 8/5/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "MyLocation.h"

@implementation MyLocation

-(id)initWithTitle:(NSString*)newTitle Location:(CLLocationCoordinate2D)location
{
    self = [super init];
    
    if (self) {
        _title = newTitle;
        _coordinate = location;
    }
    return self;
}

-(MKAnnotationView *)annotationView
{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"MyLocation"];
    annotationView.enabled = YES;
    annotationView.canShowCallout = NO;
    annotationView.image = [UIImage imageNamed:@"icoPinTicket"];
    
    return annotationView;
}

@end

