//
//  PageContentViewController.m
//  DemeDJ
//
//  Created by Moremo on 7/26/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tutorialImagesView.image = [UIImage imageNamed:self.imageFile];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
