//
//  ViewController.m
//  DemeDJ
//
//  Created by Moremo on 7/15/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ViewController ()
@end

@implementation ViewController

@synthesize tutorialImages;
@synthesize tutorialTitles;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
    tutorialImages = @[@"", @"", @"", @""];
    tutorialTitles = @[@"1", @"2", @"3", @"4"];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                            options:nil];
    
    self.pageViewController.dataSource = self;
    [[self.pageViewController view] setFrame:[[self view] bounds]];
    
    PageContentViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageViewController setViewControllers:viewControllers
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [[self view] addSubview:[self.pageViewController view]];
    [self.pageViewController didMoveToParentViewController:self];
     */
    
    /*Email
    UIButton *loginWithEmailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginWithEmailBtn setTitle:@"Log in with Email" forState:UIControlStateNormal];
    loginWithEmailBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    loginWithEmailBtn.backgroundColor = [UIColor blackColor];
    [loginWithEmailBtn sizeToFit];
    loginWithEmailBtn.center = CGPointMake(380/2, 530);
    [loginWithEmailBtn addTarget:self
                          action:@selector(buttonPressed:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginWithEmailBtn];
    */
    
    /*Facebook
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.center = CGPointMake(380/2, 580); //self.view.center;
    [self.view addSubview:loginButton];
     */
}

/*
- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.tutorialImages[index];
    pageContentViewController.titleText = self.tutorialTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound)
    {
        return nil;
    }
    
    index++;
    
    if (index == [self.tutorialTitles count])
    {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.tutorialTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
 */

//- (void)buttonPressed:(UIButton *)loginWithEmailBtn
//{
//    UIViewController *storyboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    [self presentViewController:storyboard animated:YES completion:nil];
//}

- (IBAction)loginWithFacebook:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email", @"user_friends"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    
                                    if(result.token)
                                    {
//                                        [self getFacebookProfileInfo];
                                    }
                                    
                                }
                            }];

}

- (IBAction)loginWithEmail:(id)sender
{
    UIViewController *storyboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:storyboard animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
