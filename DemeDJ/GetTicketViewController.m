//
//  GetTicketViewController.m
//  DemeDJ
//
//  Created by Moremo on 8/5/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "GetTicketViewController.h"

@interface GetTicketViewController ()
@end

@implementation GetTicketViewController

@synthesize nameEventlbl;
@synthesize nameEvent;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    nameEventlbl.text = nameEvent;
}

- (IBAction)backBtn:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
