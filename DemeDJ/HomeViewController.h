//
//  HomeViewController.h
//  DemeDJ
//
//  Created by Moremo on 7/25/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *myObject;
    NSDictionary *dictionary;
    
    NSString *event_title;
//    NSString *event_image;
    UIImage *event_image;
    NSString *event_date;
    NSString *event_price;
    NSString *event_latitude;
    NSString *event_longtitude;
    NSString *event_location;
    NSString *event_tel;
    NSString *base_url;
}

@property (strong, nonatomic) IBOutlet UITableView *homeTable;


@end
