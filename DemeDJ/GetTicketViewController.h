//
//  GetTicketViewController.h
//  DemeDJ
//
//  Created by Moremo on 8/5/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetTicketViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *nameEventlbl;

@property (nonatomic, strong) NSString *nameEvent;

- (IBAction)backBtn:(id)sender;

@end
