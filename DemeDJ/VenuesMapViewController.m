//
//  VenuesMapViewController.m
//  DemeDJ
//
//  Created by Moremo on 8/5/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "VenuesMapViewController.h"

#import "MyLocation.h"

@interface VenuesMapViewController ()
@end

@implementation VenuesMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.venuesMap.delegate = self;
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(13.7468,100.535);
    MyLocation *annotation = [[MyLocation alloc]initWithTitle:@"" Location:coordinate];
    
    CLLocationCoordinate2D coordinate1 = CLLocationCoordinate2DMake(13.6683,100.634);
    MyLocation *annotation1 = [[MyLocation alloc]initWithTitle:@"" Location:coordinate1];
    
    CLLocationCoordinate2D coordinate2 = CLLocationCoordinate2DMake(13.7468,100.5392);
    MyLocation *annotation2 = [[MyLocation alloc]initWithTitle:@"" Location:coordinate2];
    
    CLLocationCoordinate2D coordinate3 = CLLocationCoordinate2DMake(13.7442,100.5444);
    MyLocation *annotation3 = [[MyLocation alloc]initWithTitle:@"" Location:coordinate3];

    [self.venuesMap addAnnotation:annotation];
    [self.venuesMap addAnnotation:annotation1];
    [self.venuesMap addAnnotation:annotation2];
    [self.venuesMap addAnnotation:annotation3];

    [self.venuesMap setRegion:MKCoordinateRegionMakeWithDistance(annotation2.coordinate, 500.0, 500.0)];
}

- (IBAction)backBtn:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
