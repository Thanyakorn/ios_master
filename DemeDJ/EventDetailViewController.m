//
//  DetailEventsViewController.m
//  DemeDJ
//
//  Created by Moremo on 8/4/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import "EventDetailViewController.h"

#import "GetTicketViewController.h"
#import "MyLocation.h"

@interface EventDetailViewController ()
@end

@implementation EventDetailViewController

@synthesize detailScll;
@synthesize eventMap;

@synthesize nameEvent;
@synthesize nameEventlbl;
@synthesize dateEvent;
@synthesize dateEventlbl;
@synthesize timeEvent;
@synthesize timeEventlbl;
@synthesize priceEvent;
@synthesize priceEventlbl;
@synthesize locationEvent;
@synthesize locationEventlbl;
@synthesize telEvent;
@synthesize telEventlbl;

@synthesize latEvent;
@synthesize longEvent;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [detailScll setScrollEnabled:YES];
    [detailScll setContentSize:CGSizeMake(375, 1300)];
    
    nameEventlbl.text = nameEvent;
    dateEventlbl.text = dateEvent;
    timeEventlbl.text = timeEvent;
    priceEventlbl.text = priceEvent;
    
    locationEventlbl.text = locationEvent;
    telEventlbl.text = telEvent;
    
    NSLog(@"%@%@",latEvent,longEvent);
    
    self.eventMap.delegate = self;
    
//    EventAnnotation *annotation = [EventAnnotation new];
//    annotation.coordinate = CLLocationCoordinate2DMake(13.7468,100.535);
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(13.7468,100.535);
    MyLocation *annotation = [[MyLocation alloc]initWithTitle:@"" Location:coordinate];
    
    [self.eventMap addAnnotation:annotation];
    
    [self.eventMap setRegion:MKCoordinateRegionMakeWithDistance(annotation.coordinate, 1000.0, 1000.0)];
}

- (IBAction)getTicketBtn:(id)sender;
{
    GetTicketViewController *getticketView = [self.storyboard instantiateViewControllerWithIdentifier:@"GetTicketViewController"];

    getticketView.nameEvent = self.nameEvent;
    
    [self presentViewController:getticketView animated:YES completion:nil];
}

- (IBAction)backBtn:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
