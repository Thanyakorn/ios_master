//
//  LoginViewController.h
//  DemeDJ
//
//  Created by Moremo on 7/22/2559 BE.
//  Copyright © 2559 T3NET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;

- (IBAction)loginBtn:(id)sender;

- (IBAction)backBtn:(id)sender;

@end
